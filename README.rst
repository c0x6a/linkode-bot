===========
Linkode bot
===========

A telegram bot to use Linkode_.


This bot works using a conversation flow, see the flowchart_ of the 
conversation.

Try the bot on telegram: https://t.me/linkode_bot


Do you want to host it yourself?
++++++++++++++++++++++++++++++++

If you want to have the bot running on your own instance, installing 
it is very easy, this bot was written using Python3.7.

Installation:
-------------

#. Create a virtual environment.
#. Install requirements: ``pip install -U -r requirements.txt``.
#. Set the bot token in an environment variable named 
   ``LINKODE_BOT_TOKEN``.
#. Run it with ``python main.py``


.. _Linkode: https://linkode.org/
.. _flowchart: flowchart.md
