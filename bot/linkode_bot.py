"""
Telegram bot to use Linkode
"""

import logging
from urllib import parse
from uuid import uuid4

from telegram import (
    InlineKeyboardButton, InlineKeyboardMarkup, InlineQueryResultArticle,
    InputTextMessageContent, ParseMode, Update,
)
from telegram.ext import (
    CallbackContext, CallbackQueryHandler, CommandHandler, ConversationHandler,
    Filters, InlineQueryHandler, MessageHandler, Updater,
)

import kilink
from bot import keyboards, version

HELP_TEXT = (
    f"**Linkode bot v{version}**\n\n"
    "Bot to help you upload code snippets to https://linkode.org\n\n"
    "With this bot you can:\n"
    " - Create a new linkode using: `📋New`\n"
    " - Add a new child node to a linkode with: `📝Add child`\n\n"
    "Then you can share the resulting link so others can see it."
)

SELECT_TEXT_TYPE_MESSAGE = (
    'Select the `text type` that you want to use to format your upload.'
)

(
    CHOOSE_ACTION,
    NEW_PASTE,
    ADD_CHILD_PASTE,
    VERIFY_PARENT,
    SELECT_TEXT_TYPE,
    UPLOAD_CODE,
) = range(6)


class LinkodeBot:
    def __init__(self, token):
        self.logger = logging.getLogger(__name__)
        self.updater = self._init_updater(token)

    def _init_updater(self, token):
        updater = Updater(token=token, use_context=True)
        dispatcher = updater.dispatcher

        conv_handler = ConversationHandler(
            entry_points=[
                CommandHandler('start', self.choose_action),
                MessageHandler(
                    Filters.regex(keyboards.new_paste_key),
                    self.choose_text_type,
                ),
                MessageHandler(
                    Filters.regex(keyboards.add_child_key),
                    self.add_child_to_node,
                ),
            ],
            states={
                CHOOSE_ACTION: [
                    # Create new paste
                    CommandHandler('new', self.choose_text_type),
                    MessageHandler(
                        Filters.regex(keyboards.new_paste_key),
                        self.choose_text_type,
                    ),
                    # Add a child to a node.
                    CommandHandler('child', self.add_child_to_node),
                    MessageHandler(
                        Filters.regex(keyboards.add_child_key),
                        self.add_child_to_node,
                    ),
                ],
                ADD_CHILD_PASTE: [
                    MessageHandler(
                        Filters.text,
                        self.choose_text_type,
                    ),
                ],
                VERIFY_PARENT: [
                    MessageHandler(
                        Filters.text,
                        self.verify_parent_node,
                    ),
                ],
                SELECT_TEXT_TYPE: [
                    MessageHandler(
                        Filters.text,
                        self.choose_text_type,
                    ),
                ],
                UPLOAD_CODE: [
                    MessageHandler(
                        Filters.text,
                        self.upload_code,
                        pass_user_data=True,
                    ),
                    MessageHandler(
                        ~Filters.text,
                        self.other_than_text,
                    ),
                ],
            },
            fallbacks=[
                MessageHandler(
                    Filters.text,
                    self.fallback,
                    pass_user_data=True,
                )
            ],
        )

        help_handler = CommandHandler('help', self.help)
        unknown_handler = MessageHandler(Filters.command, self.unknown)

        dispatcher.add_handler(conv_handler)
        dispatcher.add_handler(help_handler)
        dispatcher.add_handler(unknown_handler)
        dispatcher.add_handler(CallbackQueryHandler(self.selected_text_type))
        dispatcher.add_handler(InlineQueryHandler(self.inline_query))

        return updater

    def start_bot(self):
        self.updater.start_polling()
        self.logger.info('bot started!')
        self.updater.idle()

    def help(self, update: Update, context: CallbackContext):
        update.message.reply_markdown(
            text=HELP_TEXT,
            reply_markup=keyboards.start_keyboard(),
        )

    def choose_action(self, update: Update, context: CallbackContext):
        update.message.reply_markdown(
            text='Please choose an action from the buttons below.',
            reply_markup=keyboards.start_keyboard(),
        )
        return CHOOSE_ACTION

    def verify_parent_node(self, update: Update, context: CallbackContext):
        if update.message.text == keyboards.new_paste_key:
            context.user_data.clear()
            return self.choose_text_type(update, context)
        if kilink.utils.verify_linkode(update.message.text):
            context.user_data['linkode_parent_id'] = update.message.text
            return self.choose_text_type(update, context)
        update.message.reply_text(
            "The linkode id is not valid or doesn't exist, try again."
        )
        return self.add_child_to_node(update, context)

    def add_child_to_node(self, update: Update, context: CallbackContext):
        update.message.reply_text('Please enter the parent Linkode id:')
        return VERIFY_PARENT

    def choose_text_type(self, update: Update, context: CallbackContext):
        context.user_data['text_type_buttons'] = update.message.reply_markdown(
            text=SELECT_TEXT_TYPE_MESSAGE,
            reply_markup=keyboards.text_types_inline(),
        )
        return UPLOAD_CODE

    def other_than_text(self, update: Update, context: CallbackContext):
        update.message.reply_text('Please upload only text.')
        return UPLOAD_CODE

    def upload_code(self, update: Update, context: CallbackContext):
        text_type = context.user_data.get('text_type')
        if text_type not in kilink.text_types:
            text_type_buttons = context.user_data['text_type_buttons']
            text_type_buttons.edit_text(
                text='Please choose **one** `text type` from the buttons '
                     'listed in the message.',
                parse_mode=ParseMode.MARKDOWN
            )
            return self.choose_text_type(update, context)

        try:
            linkode_url = kilink.utils.upload_paste(
                text_type,
                update.message.text,
                context.user_data.get('linkode_parent_id'),
            )
        except kilink.exceptions.KilinkException as ex:
            update.message.reply_text(text=str(ex))
            return self.choose_action(update, context)

        linkode_url_parsed = parse.quote_plus(linkode_url)
        share_button = InlineKeyboardMarkup([
            [
                InlineKeyboardButton(
                    'Share', url=f'https://t.me/share/url?url={linkode_url_parsed}'
                )
            ],
        ])
        update.message.reply_text(
            text=f'Your new Linkode URL:\n{linkode_url}',
            reply_markup=share_button
        )
        context.user_data.clear()

        return self.flow_end(update, context)

    def selected_text_type(self, update: Update, context: CallbackContext):
        query = update.callback_query
        text_type = query.data if query.data != 'auto' else ''
        text = f'🔽🔽Paste your `{text_type}` code now.🔽🔽'
        query.edit_message_text(text=text, parse_mode=ParseMode.MARKDOWN)
        context.user_data['text_type'] = query.data

    def flow_end(self, update: Update, context: CallbackContext):
        update.message.reply_markdown(
            text='Please choose an action from the buttons below.',
            reply_markup=keyboards.start_keyboard(),
        )
        context.user_data.clear()
        return ConversationHandler.END

    def unknown(self, update: Update, context: CallbackContext):
        update.message.reply_text(
            text="Sorry, I didn't understand that command.\n"
                 "Try again or send /help",
        )

    def fallback(self, update: Update, context: CallbackContext):
        update.message.reply_markdown(
            "I don't know what to do, please choose an action from the "
            "buttons showed below or send /help."
        )
        return self.choose_action(update, context)

    def inline_query(self, update: Update, context: CallbackContext):
        query = update.inline_query.query
        upload_result = self._upload_from_inline(query)
        results = [
            InlineQueryResultArticle(
                id=uuid4(),
                title=upload_result,
                input_message_content=InputTextMessageContent(upload_result)
            ),
        ]
        update.inline_query.answer(results)

    def _upload_from_inline(self, content):
        if not content:
            return "Paste your code to upload to Linkode"
        try:
            linkode_url = kilink.utils.upload_paste(
                'auto',
                content,
            )
        except kilink.exceptions.KilinkException as ex:
            linkode_url = f"There was an error:\n{ex}"
        return linkode_url
