from telegram import (
    InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup,
)

import kilink
from bot import utils

new_paste_key = '📋New'

add_child_key = '📝Add child'


def start_keyboard():
    return ReplyKeyboardMarkup(
        keyboard=[(new_paste_key, add_child_key)],
        resize_keyboard=True,
        one_time_keyboard=True,
    )


def text_types_inline():
    button_groups = [
        group for group in utils.grouper(kilink.text_types, 4, '-')
    ]
    text_types_keyboard = []
    for button_group in button_groups:
        text_type_buttons = (
            InlineKeyboardButton(
                button, callback_data=button,
            )
            for button in button_group if button != '-'
        )
        text_types_keyboard.append(text_type_buttons)

    keyboard = InlineKeyboardMarkup([
        *text_types_keyboard,
    ])
    return keyboard
