import logging

import requests

from kilink.exceptions import KilinkException

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)

LINKODE_HOST = 'https://linkode.org'


def _is_response_sucess(response_code):
    return 200 <= response_code <= 299


def upload_content(endpoint, data):
    """Upload user content to Linkode server."""
    try:
        response = requests.post(f'{LINKODE_HOST}/{endpoint}', data=data)
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
        raise KilinkException('Error conecting to Linkode server')

    if _is_response_sucess(response.status_code):
        linkode_id = response.json().get('linkode_id')
        revision = response.json().get('revno')
        final_id = linkode_id if linkode_id == revision else revision
        return f'{LINKODE_HOST}/#{final_id}'
    else:
        raise KilinkException(f'Error from server: '
                              f'{response.status_code} - {response.reason}')


def upload_paste(text_type, content, parent_id=None):
    """Upload a paste to Linkode."""
    if parent_id:
        paste_endpoint = f'api/1/linkodes/{parent_id}'
        data = {'text_type': text_type, 'content': content, 'parent': parent_id}
    else:
        paste_endpoint = 'api/1/linkodes/'
        data = {'text_type': text_type, 'content': content}
    return upload_content(paste_endpoint, data)


def verify_linkode(linkode_id):
    """Verify that a linkode exists"""
    response = requests.get(f'{LINKODE_HOST}/api/1/linkodes/{linkode_id}')
    return _is_response_sucess(response.status_code)
