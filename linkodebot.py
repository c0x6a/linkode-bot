#!/usr/bin/env python3
"""
Telegram bot to use https://linkode.org
"""
import logging

from environs import Env

from bot.linkode_bot import LinkodeBot

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)

env = Env()

env.read_env()

if __name__ == '__main__':
    logger.info('starting...')
    linkode_bot = LinkodeBot(env('LINKODE_BOT_TOKEN'))
    linkode_bot.start_bot()
    logger.info('stopped!')
