Linkode telegram bot flowchart:
-------------------------------

```mermaid
graph TD
    A(Start) --> B{Choose action}
    B --> |Add child| D(Insert parent id)
    B --> |New| E(Choose text type)
    E --> F(Paste code to upload)
    D --> E
    F --> Z(End)
```
